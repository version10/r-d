var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var less = require('gulp-less');
var cssnano = require('gulp-cssnano');
var rename = require("gulp-rename");
var plumber = require('gulp-plumber');
var order = require('gulp-order');
var sourcemaps = require('gulp-sourcemaps');
var es = require('event-stream');
var browserSync = require('browser-sync');
var babel = require('gulp-babel');
var gutil = require('gulp-util');

var url = 'local.version10.ca/r-d';
var basePath = '';
var mod = ["","RainTransition","LightParallax","SortableManager"];
var modLength = mod.length;
var tasks = ['watch'];
var watchs = [];

function getModPath(mod){
	return "src/com/version10/"+mod+"/";
}

function buildTask(mod){
	
	if(mod == ""){
		gulp.task(mod+'scripts', function(){
			return gulp.src([mod+'scripts/js/**/*.js'])
				.pipe(plumber())
				.pipe(order([
					mod+'scripts/js/**/*.js'
				]))
				.pipe(sourcemaps.init())
				.pipe(babel({
					presets: ['es2015']/*,
					plugins: ['transform-es2015-modules-systemjs']*/
				}))
				.pipe(concat('scripts.min.js'))
				.pipe(uglify({ preserveComments:'license' }).on('error', gutil.log))
				.pipe(sourcemaps.write('./'))
				.pipe(gulp.dest(mod+'scripts/dist/'))
				.pipe(browserSync.reload({stream:true}));
		});
	}else{
		gulp.task(mod+'scripts', function(){
			return gulp.src([getModPath(mod)+'scripts/js/**/*.js','!'+getModPath(mod)+'scripts/js/main.js'])
				.pipe(plumber())
				.pipe(order([
					getModPath(mod)+'scripts/js/**/*.js'
				]))
				.pipe(sourcemaps.init())
				.pipe(babel({
					presets: ['es2015']/*,
					plugins: ['transform-es2015-modules-systemjs']*/
				}))
				.pipe(concat('scripts.min.js'))
				.pipe(uglify({ preserveComments:'license' }).on('error', gutil.log))
				.pipe(sourcemaps.write('./'))
				.pipe(gulp.dest(getModPath(mod)+'scripts/dist/'))
				.pipe(rename(mod+'.min.js'))
				.pipe(gulp.dest(getModPath(mod)+'build/'))
				.pipe(browserSync.reload({stream:true}));
		});
	}
	
	if(mod == ""){
		gulp.task(mod+'less', function() {
			return gulp.src(mod+'styles/less/styles.less')
				.pipe(plumber())
				.pipe(sourcemaps.init())
				.pipe(less())
				.pipe(cssnano({zindex:false}))
				.pipe(rename('styles.min.css'))
				.pipe(sourcemaps.write('./'))
				.pipe(gulp.dest(mod+'styles/css/'))
				.pipe(browserSync.reload({stream:true}));
		});
	}else{
		gulp.task(mod+'less', function() {
			return es.merge(
				gulp.src(getModPath(mod)+'styles/less/main.less')
				.pipe(plumber())
				.pipe(sourcemaps.init())
				.pipe(less())
				.pipe(cssnano({zindex:false}))
				.pipe(rename('main.min.css'))
				.pipe(sourcemaps.write('./'))
				.pipe(gulp.dest(getModPath(mod)+'styles/css/')),

				gulp.src(getModPath(mod)+'styles/less/styles.less')
				.pipe(plumber())
				.pipe(sourcemaps.init())
				.pipe(less())
				.pipe(cssnano({zindex:false}))
				.pipe(rename(mod+'.min.css'))
				.pipe(sourcemaps.write('./'))
				.pipe(gulp.dest(getModPath(mod)+'styles/css/'))
				.pipe(rename(mod+'.min.css'))
				.pipe(gulp.dest(getModPath(mod)+'build/'))
				.pipe(browserSync.reload({stream:true}))
			);			
		});
	}

	tasks.push(mod+'scripts');
	tasks.push(mod+'less');
	if(mod == ""){
		watchs.push(mod+'styles/css/**/*.css');
		watchs.push(mod+'scripts/**/*.js');
	}else{
		watchs.push(getModPath(mod)+'styles/css/**/*.css');
		watchs.push(getModPath(mod)+'scripts/**/*.js');
	}
}

for(var i = 0; i < modLength; i++){
	buildTask(mod[i]);
}

gulp.task('browser-sync', function() {
    browserSync.init(watchs, {
    	proxy: url
    });
});

// Reload all Browsers
gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('watch', ['browser-sync'], function() {
	for(var i = 0; i < modLength; i++){
		if(mod[i] == ""){
			gulp.watch(mod[i] + 'styles/less/**/*.less', [mod[i] +'less']);
			gulp.watch(mod[i] + 'scripts/**/*.js', [mod[i] +'scripts']);
		}else{
			gulp.watch(getModPath(mod[i]) + 'styles/less/**/*.less', [mod[i] +'less']);
			gulp.watch(getModPath(mod[i]) + 'scripts/**/*.js', [mod[i] +'scripts']);
		}
	}
});

// Default Task
gulp.task('default', tasks);