﻿<?php 
	$v = "?v=2";
?>
<!doctype html>
<html lang="fr">

	<head>
		<meta charset="utf-8"/>
		<meta name="msapplication-tap-highlight" content="no">
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<meta name="" content="content">
		<meta name="description" content=".">
		<meta name="keywords" content="" />
		<title>R&D</title>

		<!-- Stylesheet -->
		<link rel="stylesheet" href="styles/css/styles.min.css<?= $v ?>" type="text/css" media="screen, print"/>
	</head>

	<body>
		<h1 id="title-page"><span class="logo"></span>Recherche & développement</h1>

		<nav id="filters-menu" data-filters-menu>
			<div class="seach">Recherche: <input type="search" name="search" placeholder="" data-input-search /></div>
		</nav>
			
		<section id="container">
			<ul class="menu grid" data-isotope>
				<li class="grid-sizer"></li>

				<li class="grid-item action" data-href="src/com/version10/LightParallax">
					<div class="test-page">
						<div class="title">LightParallax</div>
						<div class="description">Module parallax simple en JavaScript pour le nouveau site de v10.</div>
						<div class="tags">
							<div class="tag js">JS</div>
							<div class="tag es6">ES6</div>
						</div>
					</div>
				</li>
				<li class="grid-item action" data-href="https://github.com/version10/isogrid">
					<div class="test-page">
						<div class="title">Isogrid</div>
						<div class="description">Module de de contenu appele via webservies a l'aide de Isotope</div>
						<div class="tags">
							<div class="tag js">JS</div>
							<div class="tag es6">ES6</div>
						</div>
					</div>
				</li>
				<li class="grid-item action" data-href="src/com/version10/RainTransition">
					<div class="test-page">
						<div class="title">RainTransition</div>
						<div class="description">Module de pluie en JavaScript pour le nouveau site de v10.</div>
						<div class="tags">
							<div class="tag js">JS</div>
							<div class="tag es6">ES6</div>
						</div>
					</div>
				</li>

				<li class="grid-item action" data-href="src/com/version10/SortableManager">
					<div class="test-page">
						<div class="title">SortableManager</div>
						<div class="description">Module Sortable avancé en JavaScript pour Press Hopper.</div>
						<div class="tags">
							<div class="tag js">JS</div>
							<div class="tag es6">ES6</div>
						</div>
					</div>
				</li>
			</ul>
		</section>

		<!-- Global Vars -->
		<script>
			var VERSION = "<?= $v ?>";
		</script>
		<!-- Global Vars -->

		<!-- Depencencies R&D -->
		<script src="bower_components/jquery/dist/jquery.min.js<?= $v ?>"></script>
		<script src="bower_components/isotope/dist/isotope.pkgd.min.js<?= $v ?>"></script>
		<script src="bower_components/syntaxhighlighter/scripts/XRegExp.js<?= $v ?>"></script>
		<script src="bower_components/syntaxhighlighter/scripts/shCore.js<?= $v ?>"></script>
		<script src="bower_components/syntaxhighlighter/scripts/shBrushJScript.js<?= $v ?>"></script>
		<script src="bower_components/syntaxhighlighter/scripts/shBrushPhp.js<?= $v ?>"></script>
		<script src="bower_components/syntaxhighlighter/scripts/shBrushCss.js<?= $v ?>"></script>
		<script src="bower_components/syntaxhighlighter/scripts/shBrushXml.js<?= $v ?>"></script>

		<!--<script src="bower_components/system.js/dist/system.js<?= $v ?>"></script>-->

		<script>
			//System.config({
				// or 'traceur' or 'typescript'
				//transpiler: 'babel',
				// or traceurOptions or typescriptOptions
				//babelOptions: {}
			//});

			/* Scripts R&D */
			//System.import("scripts/dist/scripts.min.js<?= $v ?>");
		</script>

		<!-- Scripts R&D -->
		<script type="text/javascript" src="scripts/dist/scripts.min.js<?= $v ?>"></script>
	</body>
</html>
