﻿<?php 
	$v = "?v=1";
?>
<!doctype html>
<html lang="fr">

	<head>
		<meta charset="utf-8"/>
		<meta name="msapplication-tap-highlight" content="no">
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<meta name="" content="content">
		<meta name="description" content=".">
		<meta name="keywords" content="" />
		<title>R&D</title>

		<!-- Stylesheet -->
		<link rel="stylesheet" href="../../../../styles/css/styles.min.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="../../../../bower_components/syntaxhighlighter/styles/shCoreRDark.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="../../../../bower_components/syntaxhighlighter/styles/shThemeRDark.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="styles/css/RainTransition.min.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="styles/css/main.min.css<?= $v ?>" type="text/css" media="screen, print"/>
	</head>

	<body>
		<h1 id="title-page"><span class="logo"></span>RainTransition<span class="btn-back" data-href="./../../../../"></span></h1>

		<nav id="filters-menu" data-filters-menu></nav>
			
		<section id="container">
			<!-- Quick start -->
			<h2 class="title-section">Quick start</h2>
			<div class="box">

				<h3 class="title-sub">Dependencies</h3>

				<div class="sublist">
					jQuery / GSAP
					<div class="code">
						<pre class="brush: js">
							/**
							* Run in bower
							*/
							bower install jquery gsap --save
						</pre>
					</div>
				</div>

				<h3 class="title-sub">Download</h3>

				<div class="sublist">
					Build
					<div class="code">
						<pre class="brush: js">
							/**
							* Run in bower
							*/
							bower install https://YOUR_NAME@bitbucket.org/version10/r-d.git#RainTransition --save
						</pre>
					</div>
				</div>

				<h3 class="title-sub">Installation</h3>

				<div class="sublist">
					Add script
					<div class="code">
						<script type="syntaxhighlighter" class="brush: html"><![CDATA[
							/**
							* Dependencies
							*/
							<script src="bower_components/jquery/dist/jquery.min.js" />
							<script src="bower_components/gsap/src/minified/TweenMax.min.js" />
							<script src="bower_components/gsap/src/minified/easing/EasePack.min.js" />
							<script src="bower_components/gsap/src/minified/plugins/CSSPlugin.min.js" />

							/**
							* RainTransition script
							*/
							<script type="text/javascript" src="RainTransition.min.js" />
						]]></script>
					</div>
				</div>

				<div class="sublist">
					Add style
					<div class="code">
						<script type="syntaxhighlighter" class="brush: html"><![CDATA[
							/**
							* Inner head
							*/
							<link rel="stylesheet" href="RainTransition.min.css" type="text/css" media="screen, print" />
						]]></script>
					</div>
				</div>

				<div class="sublist">
					HTML
					<div class="code">
						<script type="syntaxhighlighter" class="brush: html"><![CDATA[
							/**
							* Inner body
							*/
							<div class="rain-transition" data-rain-transition></div>
						]]></script>
					</div>
				</div>

				<h3 class="title-sub">First Call</h3>

				<div class="code">
					<script type="syntaxhighlighter" class="brush: js"><![CDATA[
						/**
						* Initialisation
						*/
						var rainTransition = new RainTransition($("[data-rain-transition]"));
					]]></script>
				</div>
			</div>

			<!-- Options -->
			<h2 class="title-section">Options</h2>
			<div class="box">
				<h3 class="title-sub">Usage</h3>

				<div class="sublist">
					Exemple
					<div class="code">
						<script type="syntaxhighlighter" class="brush: js"><![CDATA[
						/**
						* Exemple
						*/
						var rainTransition = new RainTransition($("[data-rain-transition]"), {
							startColor:"#ef662f",
							stopColor:"#d72355",
							minWidth:10,
							maxWidth:50,
							delay:1
						});
					]]></script>
					</div>
				</div>

				<h3 class="title-sub">Parameters</h3>

				<div class="sublist">
					Options of your RainTransition
					<div class="code">
						<table class="params">
							<thead>
								<tr>
									<th>Name</th>
									<th>Type</th>
									<th>Default</th>
									<th class="last">Description</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="name"><code>startColor</code></td>
									<td class="type">string</td>
									<td class="default">'#ef662f'</td>
									<td class="description">the start color of the rain gradient background</td>
								</tr>
								<tr>
									<td class="name"><code>stopColor</code></td>
									<td class="type">string</td>
									<td class="default">'#ef662f'</td>
									<td class="description">the stop color of the rain gradient background</td>
								</tr>
								<tr>
									<td class="name"><code>minWidth</code></td>
									<td class="type">number</td>
									<td class="default">10</td>
									<td class="description">the minimum width of the Rain</td>
								</tr>
								<tr>
									<td class="name"><code>maxWidth</code></td>
									<td class="type">number</td>
									<td class="default">50</td>
									<td class="description">the maximum width of the Rain</td>
								</tr>
								<tr>
									<td class="name"><code>delay</code></td>
									<td class="type">number</td>
									<td class="default">1</td>
									<td class="description">the maximum delay between rains</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- Methods -->
			<h2 class="title-section">Methods</h2>
			<ul class="menu grid" data-isotope>
				<li class="grid-sizer"></li>
				<li class="grid-item methods" data-method="rainIn">
					<div class="test-page">
						<div class="title">Method rainIn</div>
						<div class="statement">.rainIn( speed, ease, onComplete )</div>
						<div class="sublist">
							Parameters
							<div class="code">
								<table class="params">
									<thead>
										<tr>
											<th>Name</th>
											<th>Type</th>
											<th>Default</th>
											<th class="last">Description</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="name"><code>speed</code></td>
											<td class="type">number</td>
											<td class="default">.5</td>
											<td class="description">Speed animation of the rain</td>
										</tr>
										<tr>
											<td class="name"><code>ease</code></td>
											<td class="type">Ease GSAP Object</td>
											<td class="default">Power0.easeNone</td>
											<td class="description">the ease type animation</td>
										</tr>
										<tr>
											<td class="name"><code>onComplete</code></td>
											<td class="type">Function</td>
											<td class="default">null</td>
											<td class="description">callback event on complete</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									rainTransition.rainIn(.5);
								]]></script>
							</div>
						</div>
					</div>
				</li>
				<li class="grid-item methods" data-method="rainOut">
					<div class="test-page">
						<div class="title">Method rainOut</div>
						<div class="statement">.rainOut( speed, ease, onComplete )</div>
						<div class="sublist">
							Parameters
							<div class="code">
								<table class="params">
									<thead>
										<tr>
											<th>Name</th>
											<th>Type</th>
											<th>Default</th>
											<th class="last">Description</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="name"><code>speed</code></td>
											<td class="type">number</td>
											<td class="default">.5</td>
											<td class="description">Speed animation of the rain</td>
										</tr>
										<tr>
											<td class="name"><code>ease</code></td>
											<td class="type">Ease GSAP Object</td>
											<td class="default">Power0.easeNone</td>
											<td class="description">the ease type animation</td>
										</tr>
										<tr>
											<td class="name"><code>onComplete</code></td>
											<td class="type">Function</td>
											<td class="default">null</td>
											<td class="description">callback event on complete</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									rainTransition.rainOut(.5);
								]]></script>
							</div>
						</div>
					</div>
				</li>
				<li class="grid-item methods" data-method="fadeToA">
					<div class="test-page">
						<div class="title">Method fadeTo</div>
						<div class="statement">.fadeTo( opacity, speed, ease, onComplete )</div>
						<div class="sublist">
							Parameters
							<div class="code">
								<table class="params">
									<thead>
										<tr>
											<th>Name</th>
											<th>Type</th>
											<th>Default</th>
											<th class="last">Description</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="name"><code>opacity</code></td>
											<td class="type">number</td>
											<td class="default">1</td>
											<td class="description">The new opacity</td>
										</tr>
										<tr>
											<td class="name"><code>speed</code></td>
											<td class="type">number</td>
											<td class="default">.5</td>
											<td class="description">Speed animation of the fade</td>
										</tr>
										<tr>
											<td class="name"><code>ease</code></td>
											<td class="type">Ease GSAP Object</td>
											<td class="default">Power0.easeNone</td>
											<td class="description">the ease type animation</td>
										</tr>
										<tr>
											<td class="name"><code>onComplete</code></td>
											<td class="type">Function</td>
											<td class="default">null</td>
											<td class="description">callback event on complete</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									rainTransition.fadeTo(1, .5);
								]]></script>
							</div>
						</div>
					</div>
				</li>
				<li class="grid-item methods" data-method="fadeToB">
					<div class="test-page">
						<div class="title">Method fadeTo</div>
						<div class="statement">.fadeTo( opacity, speed, ease, onComplete )</div>
						<div class="sublist">
							Parameters
							<div class="code">
								<table class="params">
									<thead>
										<tr>
											<th>Name</th>
											<th>Type</th>
											<th>Default</th>
											<th class="last">Description</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="name"><code>opacity</code></td>
											<td class="type">number</td>
											<td class="default">1</td>
											<td class="description">The new opacity</td>
										</tr>
										<tr>
											<td class="name"><code>speed</code></td>
											<td class="type">number</td>
											<td class="default">.5</td>
											<td class="description">Speed animation of the fade</td>
										</tr>
										<tr>
											<td class="name"><code>ease</code></td>
											<td class="type">Ease GSAP Object</td>
											<td class="default">Power0.easeNone</td>
											<td class="description">the ease type animation</td>
										</tr>
										<tr>
											<td class="name"><code>onComplete</code></td>
											<td class="type">Function</td>
											<td class="default">null</td>
											<td class="description">callback event on complete</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									rainTransition.fadeTo(.7, .5);
								]]></script>
							</div>
						</div>
					</div>
				</li>
				<li class="grid-item methods" data-method="showAllRains">
					<div class="test-page">
						<div class="title">Method showAllRains</div>
						<div class="statement">.showAllRains( )</div>
						<div class="sublist"></div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									rainTransition.showAllRains();
								]]></script>
							</div>
						</div>
					</div>
				</li>
				<li class="grid-item methods" data-method="hideAllRains">
					<div class="test-page">
						<div class="title">Method hideAllRains</div>
						<div class="statement">.hideAllRains( )</div>
						<div class="sublist"></div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									rainTransition.hideAllRains();
								]]></script>
							</div>
						</div>
					</div>
				</li>
			</ul>

			<!-- Events -->
			<h2 class="title-section">Events</h2>
			<ul class="menu grid" data-isotope>
				<li class="grid-sizer"></li>
				<li class="grid-item events" data-event="onRainInComplete">
					<div class="test-page">
						<div class="title">Event onRainInComplete</div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									$(rainTransition).on("onRainInComplete", function(e){});
								]]></script>
							</div>
						</div>
					</div>
				</li>
				<li class="grid-item events" data-event="onRainOutComplete">
					<div class="test-page">
						<div class="title">Event onRainOutComplete</div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									$(rainTransition).on("onRainOutComplete", function(e){});
								]]></script>
							</div>
						</div>
					</div>
				</li>
				<li class="grid-item events" data-event="onFadeComplete">
					<div class="test-page">
						<div class="title">Event onFadeComplete</div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									$(rainTransition).on("onFadeComplete", function(e){});
								]]></script>
							</div>
						</div>
					</div>
				</li>
			</ul>
			

		</section>

		<div class="rain-transition" data-rain-transition></div>

		<!-- Global Vars -->
		<script>
			var VERSION = "<?= $v ?>";
		</script>
		<!-- Global Vars -->

		

		<!-- Depencencies -->
		<script src="../../../../bower_components/jquery/dist/jquery.min.js<?= $v ?>"></script>

		<!-- Depencencies R&D -->
		<script src="../../../../bower_components/isotope/dist/isotope.pkgd.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/XRegExp.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shCore.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushJScript.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushPhp.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushCss.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushXml.js<?= $v ?>"></script>

		<!-- Depencencies -->
		<script src="../../../../bower_components/gsap/src/minified/TweenMax.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/gsap/src/minified/easing/EasePack.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/gsap/src/minified/plugins/CSSPlugin.min.js<?= $v ?>"></script>

		<!-- Scripts R&D -->
		<script type="text/javascript" src="../../../../scripts/dist/scripts.min.js<?= $v ?>"></script>
		<!-- Scripts -->
		<script type="text/javascript" src="scripts/dist/scripts.min.js<?= $v ?>"></script>
		<script type="text/javascript" src="scripts/js/main.js<?= $v ?>"></script>
	</body>
</html>
