class RainTransition {
	constructor(container, options = {}) {
		this.rains = [];

		this.container = container;
		this.isRainingOut = false;
		this.isRainingIn = false;
		this.tResize = null;
		
		this.options = {
			startColor:"#ef662f",
			stopColor:"#d72355",
			minWidth:10,
			maxWidth:50,
			delay:1
		};

		for(let opt in options){
			this.options[opt] = options[opt];
		}

		this.addRain();

		this.container.hide();

		$(window).on("resize", this.onResize.bind(this));
	}

	addRain(pos = 0, lastRainHeight = 0){
		if(pos >= this.container.width()) return;
		let rain = new Rain(this, this.container, pos, this.options.minWidth, this.options.maxWidth, lastRainHeight);
		this.rains.push(rain);
		this.rainsLng = this.rains.length;

		this.addRain(rain.pos+rain.size, rain.lastRainHeight);
	}

	rainIn(speed = .5, ease = Power0.easeNone, onComplete = null){
		if(this.isRainingIn) return;
		this.isRainingIn = true;
		this.isRainingOut = false;
		let done = 0;
		this.container.show();
		for(let i = 0; i < this.rainsLng; i++){
			TweenMax.killTweensOf(this.rains[i].grdCnt);
			TweenMax.to(this.rains[i].grdCnt, speed, {height:"100%", ease: ease, delay:Math.random()*this.options.delay, onComplete:() => {
				done++;
				if(done == this.rainsLng){
					this.onRainInComplete();
					if(onComplete != null) onComplete();
				}
			}});
		}
	}

	rainOut(speed = .5, ease = Power0.easeNone, onComplete = null){
		if(this.isRainingOut) return;
		this.isRainingOut = true;
		this.isRainingIn = false;
		let done = 0;
		for(let i = 0; i < this.rainsLng; i++){
			TweenMax.killTweensOf(this.rains[i].grdCnt);
			TweenMax.to(this.rains[i].grdCnt, speed, {height:"0%", ease: ease, delay:Math.random()*this.options.delay, onComplete:() => {
				done++;
				if(done == this.rainsLng){
					this.onRainOutComplete();
					if(onComplete != null) onComplete();
				}
			}});
		}
	}

	fadeTo(opacity = 1, speed = .5, ease = Power0.easeNone, onComplete = null){
		TweenMax.killTweensOf(this.container);
		TweenMax.to(this.container, speed, {opacity:opacity, ease: ease, onComplete:() => {
			this.onFadeComplete();
			if(onComplete != null) onComplete();
		}});
	}

	showAllRains(){
		this.isRainingIn = false;
		this.isRainingOut = false;
		this.container.show();
		for(let i = 0; i < this.rainsLng; i++){
			TweenMax.killTweensOf(this.rains[i].grdCnt);
			TweenMax.set(this.rains[i].grdCnt, {height:"100%"});
		}
	}

	hideAllRains(){
		this.isRainingIn = false;
		this.isRainingOut = false;
		for(let i = 0; i < this.rainsLng; i++){
			TweenMax.killTweensOf(this.rains[i].grdCnt);
			TweenMax.set(this.rains[i].grdCnt, {height:"0%"});
		}
	}

	onRainInComplete(){
		this.isRainingIn = false;
		this.isRainingOut = false;
		$(this).triggerHandler("onRainInComplete", this);
	}

	onRainOutComplete(){
		this.isRainingIn = false;
		this.isRainingOut = false;
		this.container.hide();
		$(this).triggerHandler("onRainOutComplete", this);
	}

	onFadeComplete(){
		$(this).triggerHandler("onFadeComplete", this);
	}

	onResize(){

		var _this = this;

		if(this.tResize != null){
			clearTimeout(this.tResize);
		}

		this.tResize = setTimeout(function(){
			var lastRain = _this.rains[(_this.rainsLng-1)];
			_this.addRain(lastRain.pos + lastRain.size, lastRain.grdCnt.height());
			for(let i = 0; i < _this.rainsLng; i++){
				_this.rains[i].onResize();
			}
		}, 250);
	}
}