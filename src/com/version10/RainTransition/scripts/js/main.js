jQuery(document).ready(function($) {

	var rainTransition = new RainTransition($("[data-rain-transition]"), {
		startColor:"#ef662f",
    	stopColor:"#d72355",
    	minWidth:10,
		maxWidth:50
    });

	$("[data-method='rainIn']").on("click", function(){
		rainTransition.rainIn(.5, Power0.easeNone, () => {
			alert("Rain in complete !");
		});
	});

	$("[data-method='rainOut']").on("click", function(){
		rainTransition.rainOut(.5);
	});

	$("[data-method='fadeToA']").on("click", function(){
		rainTransition.fadeTo(1, .5);
	});

	$("[data-method='fadeToB']").on("click", function(){
		rainTransition.fadeTo(.7);
	});

	$("[data-method='showAllRains']").on("click", function(){
		rainTransition.showAllRains();
	});

	$("[data-method='hideAllRains']").on("click", function(){
		rainTransition.hideAllRains();
	});

	$(rainTransition).on("onRainInComplete", function(e){
		$("[data-event='onRainInComplete']").addClass('trigger-event');
		setTimeout(() => $("[data-event='onRainInComplete']").removeClass('trigger-event'), 250);
	});

	$(rainTransition).on("onRainOutComplete", function(e){
		$("[data-event='onRainOutComplete']").addClass('trigger-event');
		setTimeout(() => $("[data-event='onRainOutComplete']").removeClass('trigger-event'), 250);
	});

	$(rainTransition).on("onFadeComplete", function(e){
		$("[data-event='onFadeComplete']").addClass('trigger-event');
		setTimeout(() => $("[data-event='onFadeComplete']").removeClass('trigger-event'), 250);
	});
});