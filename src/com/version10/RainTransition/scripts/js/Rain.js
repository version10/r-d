class Rain {
	constructor(stage, cnt, pos, minWidth, maxWidth, lastRainHeight = 0) {
		this.stage = stage;
		this.cnt = cnt;
		this.pos = pos;
		this.lastRainHeight = lastRainHeight;

		this.minWidth = minWidth;
		this.maxWidth = maxWidth;

		this.size = 0;
		this.init();
	}

	init(){
		var _this = this;
		this.size = Math.round(this.minWidth + Math.random()*(this.maxWidth - this.minWidth));
		if(this.pos+this.size > this.cnt.width){
			this.size = this.cnt.width - this.pos;
		}
		
		this.grdCnt = $("<div>");
		this.grdCnt.addClass('rain');
		this.grd = $("<div>");
		this.grd.addClass('rain-grd');
		this.grdCnt.append(this.grd);
		this.cnt.append(this.grdCnt);

		TweenMax.set(this.grdCnt, {x:this.pos, width:this.size, height:((this.lastRainHeight/this.cnt.height()) * 100)+"%"});
		TweenMax.set(this.grd, {width:"100%", height:this.cnt.height()});

		this.setBGColor(this.stage.options.startColor, this.stage.options.stopColor);
	}

	setBGColor(start, stop){
		let ieFilter = 'e(%("progid:DXImageTransform.Microsoft.gradient(startColorstr=\'%d\', endColorstr=\'%d\', GradientType=0)",argb(@start),argb(@stop)))';

		this.grd.css("background", start);
		this.grd.css("background", "-webkit-gradient(linear,left bottom,left top,color-stop(0, "+start+"),color-stop(1, "+stop+"))");
		this.grd.css("background", "-ms-linear-gradient(bottom, "+start+", "+stop+")");
		this.grd.css("background", "-moz-linear-gradient(center bottom,"+start+" 0%,"+stop+" 100%)");
		this.grd.css("background", "-o-linear-gradient("+stop+", "+start+")");
		this.grd.css("filter", ieFilter);
	}

	onResize(){
		TweenMax.set(this.grd, {height:this.cnt.height()});
	}
}