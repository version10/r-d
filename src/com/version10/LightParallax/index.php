﻿<?php 
	$v = "?v=1";
?>
<!doctype html>
<html lang="fr">

	<head>
		<meta charset="utf-8"/>
		<meta name="msapplication-tap-highlight" content="no">
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<meta name="" content="content">
		<meta name="description" content=".">
		<meta name="keywords" content="" />
		<title>R&D</title>

		<!-- Stylesheet -->
		<link rel="stylesheet" href="../../../../styles/css/styles.min.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="../../../../bower_components/syntaxhighlighter/styles/shCoreRDark.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="../../../../bower_components/syntaxhighlighter/styles/shThemeRDark.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="styles/css/LightParallax.min.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="styles/css/main.min.css<?= $v ?>" type="text/css" media="screen, print"/>
	</head>

	<body>
		<h1 id="title-page"><span class="logo"></span>LightParallax<span class="btn-back" data-href="./../../../../"></span></h1>

		<nav id="filters-menu" data-filters-menu></nav>
			
		<section id="container">
			<!-- Quick start -->
			<h2 class="title-section">Quick start</h2>
			<div class="box">

				<h3 class="title-sub">Dependencies</h3>

				<div class="sublist">
					jQuery / GSAP
					<div class="code">
						<pre class="brush: js">
							/**
							* Run in bower
							*/
							bower install jquery gsap --save
						</pre>
					</div>
				</div>

				<h3 class="title-sub">Download</h3>

				<div class="sublist">
					Build
					<div class="code">
						<pre class="brush: js">
							/**
							* Run in bower
							*/
							bower install https://YOUR_NAME@bitbucket.org/version10/r-d.git#LightParallax --save
						</pre>
					</div>
				</div>

				<h3 class="title-sub">Installation</h3>

				<div class="sublist">
					Add script
					<div class="code">
						<script type="syntaxhighlighter" class="brush: html"><![CDATA[
							/**
							* Dependencies
							*/
							<script src="bower_components/jquery/dist/jquery.min.js" />
							<script src="bower_components/gsap/src/minified/TweenMax.min.js" />
							<script src="bower_components/gsap/src/minified/easing/EasePack.min.js" />
							<script src="bower_components/gsap/src/minified/plugins/CSSPlugin.min.js" />

							/**
							* LightParallax script
							*/
							<script type="text/javascript" src="LightParallax.min.js" />
						]]></script>
					</div>
				</div>

				<div class="sublist">
					Add style
					<div class="code">
						<script type="syntaxhighlighter" class="brush: html"><![CDATA[
							/**
							* Inner head
							*/
							<link rel="stylesheet" href="LightParallax.min.css" type="text/css" media="screen, print" />
						]]></script>
					</div>
				</div>

				<div class="sublist">
					HTML
					<div class="code">
						<script type="syntaxhighlighter" class="brush: html"><![CDATA[
							/**
							* Inner body
							*/
							<div class="parallax-cnt" data-light-parallax>
								<div class="light-parallax-layer" data-light-parallax-layer="1"></div>
								<div class="light-parallax-layer" data-light-parallax-layer="2"></div>
								<div class="light-parallax-layer" data-light-parallax-layer="3"></div>
							</div>
						]]></script>
					</div>
				</div>

				<h3 class="title-sub">First Call</h3>

				<div class="code">
					<script type="syntaxhighlighter" class="brush: js"><![CDATA[
						/**
						* Initialisation
						*/
						var lightParallax = new LightParallax($("[data-light-parallax-layer='1']"));


					]]></script>
				</div>
			</div>

			<!-- Options -->
			<h2 class="title-section">Options</h2>
			<div class="box">
				<h3 class="title-sub">Usage</h3>

				<div class="sublist">
					Exemple
					<div class="code">
						<script type="syntaxhighlighter" class="brush: js"><![CDATA[
						/**
						* Exemple
						*/
						var lightParallax = new LightParallax($("[data-light-parallax-layer='1']"), {
							parent:$("[data-light-parallax]"),
							active:true,
							forceX:1,
							forceY:.2,
							speed:.25,
							mouseSpeed:0.01,
							updateFrequency:60,
							ease:Power0.easeNone
						});
					]]></script>
					</div>
				</div>

				<h3 class="title-sub">Parameters</h3>

				<div class="sublist">
					Options of your RainTransition
					<div class="code">
						<table class="params">
							<thead>
								<tr>
									<th>Name</th>
									<th>Type</th>
									<th>Default</th>
									<th class="last">Description</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="name"><code>parent</code></td>
									<td class="type">object</td>
									<td class="default">$(window)</td>
									<td class="description">the parent element of all parallax layer</td>
								</tr>
								<tr>
									<td class="name"><code>active</code></td>
									<td class="type">boolean</td>
									<td class="default">false</td>
									<td class="description">is the parallax is active</td>
								</tr>
								<tr>
									<td class="name"><code>forceX</code></td>
									<td class="type">number</td>
									<td class="default">1</td>
									<td class="description">the force x of the parallax</td>
								</tr>
								<tr>
									<td class="name"><code>forceY</code></td>
									<td class="type">number</td>
									<td class="default">1</td>
									<td class="description">the force y of the parallax</td>
								</tr>
								<tr>
									<td class="name"><code>speed</code></td>
									<td class="type">number</td>
									<td class="default">.25</td>
									<td class="description">speed of the transition</td>
								</tr>
								<tr>
									<td class="name"><code>mouseSpeed</code></td>
									<td class="type">number</td>
									<td class="default">0.01</td>
									<td class="description">mouse speed update</td>
								</tr>
								<tr>
									<td class="name"><code>updateFrequency</code></td>
									<td class="type">int</td>
									<td class="default">60</td>
									<td class="description">Frames per second to update the mouse position</td>
								</tr>
								<tr>
									<td class="name"><code>ease</code></td>
									<td class="type">GSAP Ease</td>
									<td class="default">Power0.easeNone</td>
									<td class="description">ease of the transition</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- Examples -->
			<h2 class="title-section">Examples</h2>
			<div class="box">
				<div class="parallax-cnt" data-light-parallax>
					<div class="light-parallax-layer" data-light-parallax-layer="1"></div>
					<div class="light-parallax-layer" data-light-parallax-layer="2"></div>
					<div class="light-parallax-layer" data-light-parallax-layer="3"></div>
				</div>
			</div>

			<!-- Methods -->
			<h2 class="title-section">Methods</h2>
			<ul class="menu grid" data-isotope>
				<li class="grid-sizer"></li>
				<li class="grid-item methods" data-method="activeA">
					<div class="test-page">
						<div class="title">Method active</div>
						<div class="statement">.active( isActive )</div>
						<div class="sublist">
							Parameters
							<div class="code">
								<table class="params">
									<thead>
										<tr>
											<th>Name</th>
											<th>Type</th>
											<th>Default</th>
											<th class="last">Description</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="name"><code>isActive</code></td>
											<td class="type">boolean</td>
											<td class="default">false</td>
											<td class="description">active / inactive the parallax</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									lightParallax.active(true);
								]]></script>
							</div>
						</div>
					</div>
				</li>
				<li class="grid-item methods" data-method="activeB">
					<div class="test-page">
						<div class="title">Method active</div>
						<div class="statement">.active( isActive )</div>
						<div class="sublist">
							Parameters
							<div class="code">
								<table class="params">
									<thead>
										<tr>
											<th>Name</th>
											<th>Type</th>
											<th>Default</th>
											<th class="last">Description</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="name"><code>isActive</code></td>
											<td class="type">boolean</td>
											<td class="default">false</td>
											<td class="description">active / inactive the parallax</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="description">
							<div class="code">
								<script type="syntaxhighlighter" class="brush: js"><![CDATA[
									lightParallax.active(false);
								]]></script>
							</div>
						</div>
					</div>
				</li>
			</ul>

		</section>


		<!-- Global Vars -->
		<script>
			var VERSION = "<?= $v ?>";
		</script>
		<!-- Global Vars -->

		

		<!-- Depencencies -->
		<script src="../../../../bower_components/jquery/dist/jquery.min.js<?= $v ?>"></script>

		<!-- Depencencies R&D -->
		<script src="../../../../bower_components/isotope/dist/isotope.pkgd.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/XRegExp.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shCore.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushJScript.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushPhp.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushCss.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushXml.js<?= $v ?>"></script>

		<!-- Depencencies -->
		<script src="../../../../bower_components/gsap/src/minified/TweenMax.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/gsap/src/minified/easing/EasePack.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/gsap/src/minified/plugins/CSSPlugin.min.js<?= $v ?>"></script>

		<!-- Scripts R&D -->
		<script type="text/javascript" src="../../../../scripts/dist/scripts.min.js<?= $v ?>"></script>
		<!-- Scripts -->
		<script type="text/javascript" src="scripts/dist/scripts.min.js<?= $v ?>"></script>
		<script type="text/javascript" src="scripts/js/main.js<?= $v ?>"></script>
	</body>
</html>
