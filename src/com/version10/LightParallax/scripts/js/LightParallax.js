class LightParallax {
	constructor(container, options = {}) {
		this.parallax = [];

		this.container = container;
		
		this.options = {
			parent:$(window),
			active:false,
			forceX:1,
			forceY:1,
			speed:.25,
			mouseSpeed:0.01,
			updateFrequency:60
		};

		for(let opt in options){
			this.options[opt] = options[opt];
		}

		//properties
		this.cusorPosition = {x:0, y:0};
		this.nCusorPosition = {x:0, y:0};
		this.center = {x:this.options.parent.width() >> 1, y: this.options.parent.height() >> 1};
		this.prc = {x:0, y: 0};
		this.initParallax();
	}

	initParallax(){
		$(window).on("mousemove", (e) => {
			this.nCusorPosition.x = e.pageX - this.options.parent.offset().left;
			this.nCusorPosition.y = e.pageY - this.options.parent.offset().top;
		});

		setInterval(() => {
			this.update();
		}, 1000 / this.updateFrequency);
	}

	update(){
		if(!this.options.active) return

		this.cusorPosition.x = this.cusorPosition.x - (this.cusorPosition.x - this.nCusorPosition.x) * this.options.mouseSpeed;
		this.cusorPosition.y = this.cusorPosition.y - (this.cusorPosition.y - this.nCusorPosition.y) * this.options.mouseSpeed;

		this.center.x = this.options.parent.width() >> 1;
		this.center.y = this.options.parent.height() >> 1;

		this.prc.x = ((this.cusorPosition.x - this.center.x) / this.options.parent.width()) * 100;
		this.prc.y = ((this.cusorPosition.y - this.center.y) / this.options.parent.height()) * 100;

		if(this.prc.x < -50) this.prc.x = -50;
		if(this.prc.y < -50) this.prc.y = -50;
		if(this.prc.x > 50) this.prc.x = 50;
		if(this.prc.y > 50) this.prc.y = 50;
		
		TweenMax.killTweensOf(this.container);
		TweenMax.set(this.container, {x:(this.prc.x * -(this.center.x / 100) * this.options.forceX), y:(this.prc.y * -(this.center.y / 100) * this.options.forceY)});
	}

	active(isActive = false){
		this.options.active = isActive;
	}
}