jQuery(document).ready(function($) {

	TweenMax.set($("[data-light-parallax-layer]:eq(0)"), {left:200, top:15});
	TweenMax.set($("[data-light-parallax-layer]:eq(1)"), {left:500, top:15});
	TweenMax.set($("[data-light-parallax-layer]:eq(2)"), {left:600, bottom:15});

	var lightParallax1 = new LightParallax($("[data-light-parallax-layer='1']"), {
		parent:$("[data-light-parallax]"),
		active:true,
		forceX:.2,
		forceY:.1,
		speed:.25,
		mouseSpeed:0.01,
		updateFrequency:60,
		ease:Power0.easeNone
    });

    var lightParallax2 = new LightParallax($("[data-light-parallax-layer='2']"), {
		parent:$("[data-light-parallax]"),
		active:true,
		forceX:.15,
		forceY:.1,
		speed:.25,
		mouseSpeed:0.01,
		updateFrequency:60,
		ease:Power0.easeNone
    });

    var lightParallax3 = new LightParallax($("[data-light-parallax-layer='3']"), {
		parent:$("[data-light-parallax]"),
		active:true,
		forceX:.1,
		forceY:.1,
		speed:.25,
		mouseSpeed:0.01,
		updateFrequency:60,
		ease:Power0.easeNone
    });

	$("[data-method='activeA']").on("click", function(){
		lightParallax1.active(true);
		lightParallax2.active(true);
		lightParallax3.active(true);
	});

	$("[data-method='activeB']").on("click", function(){
		lightParallax1.active(false);
		lightParallax2.active(false);
		lightParallax3.active(false);
	});
});