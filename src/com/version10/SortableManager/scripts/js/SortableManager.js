class SortableManager {
	constructor(container, options = {}) {
		this.parallax = [];

		this.container = container;
		
		this.options = {
			addNewPageLink:"",
			removePage:"",
			removePageConfirmFct:null,
			autoAddNewPage:"",
			editPageLink:"",
			addNewContentLink:"",
			removeContent:"",
			removeContentConfirmFct:null,
			editContentLink:"",
			updatePagePosition:"",
			updateContentPosition:"",
			defaultPageName:""
		};

		for(let opt in options){
			this.options[opt] = options[opt];
		}

		this.initSortableGroups();

		for(let items in $("[data-sortable-manager-items]").toArray()){
			this.initSortableItems($("[data-sortable-manager-items]")[items]);
		}
	}

	initSortableGroups(){
		var self = this;
		this.sortable = Sortable.create(this.container[0], {
			animation: 500,
			draggable: '.group',
			handle: '.title',
			filter: ".add",
			ghostClass: "ghost",
			chosenClass: "chosen",
			onSort: function (evt) {
				console.log("Updage position group!");
				$("[data-sortable-manager-group='add']").appendTo($("[data-sortable-manager-group='add']").parent());
				var idPage = $(evt.item).attr("data-sortable-manager-group-id");
				var index = 0;

				var groups = $("[data-sortable-manager-group='normal']").toArray();

				for(let group in groups){					
					if($(groups[group]).attr("data-sortable-manager-group-id") == idPage){
						break;
					}

					index++;
				}

				console.log(idPage + " - " + index);

				var url = self.options.updatePagePosition.replace("__PAGE_ID__", idPage);
				url = url.replace("__INDEX__", index);
				$.ajax({
					method: "POST",
					url: url
				}).done(function() {
					console.log("update position page!");
				}).fail(function() {
					alert("Une erreur est survenue, veuillez recharger la page.")
				});
			},
			onMove: function (evt) {
				if($(evt.related).attr("data-sortable-manager-group") == "normal"){
					//continue
				}else{
					return false;
				}
			}
		});

		this.container.on("click", "[data-sortable-manager-btn-group-edit]", function(e){
			e.preventDefault();

			console.log("edit page!");
			var id = $(this).parents("[data-sortable-manager-group]").attr("data-sortable-manager-group-id");
			window.location = self.options.editPageLink.replace("__PAGE_ID__", id);
		});

		$("[data-sortable-manager-btn-group-add]").on("click", function(e){
			e.preventDefault();
			console.log("Add new page!");
			window.location = self.options.addNewPageLink;
		});

		this.container.on("click", "[data-sortable-manager-btn-item-edit]", function(e){
			e.preventDefault();
			console.log("edit content!");
			var id = $(this).parents("[data-sortable-manager-item]").attr("data-sortable-manager-item-id");
			window.location = self.options.editContentLink.replace("__CONTENT_ID__", id);
		});

		this.container.on("click", "[data-sortable-manager-btn-item-add]", function(e){
			e.preventDefault();
			console.log("add new content!");
			var id = $(this).parents("[data-sortable-manager-group]").attr("data-sortable-manager-group-id");
			window.location = self.options.addNewContentLink.replace("__PAGE_ID__", id);
		});

		this.container.on("click", "[data-sortable-manager-btn-item-remove]", function(e){
			e.preventDefault();
			var _this = this;

			if(self.options.removeContentConfirmFct != null){
				self.options.removeContentConfirmFct(function(result){
					if(result){
						var id = $(_this).parents("[data-sortable-manager-item]").attr("data-sortable-manager-item-id");
						var group = $(_this).parents("[data-sortable-manager-group]");
						var url = self.options.removeContent.replace("__CONTENT_ID__", id);

						$.ajax({
							method: "DELETE",
							url: url
						}).done(function() {
							console.log("remove content!", id);
							$(_this).parents("[data-sortable-manager-item]").remove();
						}).fail(function() {
							alert("Une erreur est survenue, veuillez recharger la page.")
						});
					}
				});
			}
		});

		this.container.on("click", "[data-sortable-manager-btn-group-remove]", function(e){
			e.preventDefault();
			var _this = this;

			if(self.options.removePageConfirmFct != null){
				self.options.removePageConfirmFct(function(result){
					if(result){
						var id = $(_this).parents("[data-sortable-manager-group]").attr("data-sortable-manager-group-id");
						var group = $(_this).parents("[data-sortable-manager-group]");

						var url = self.options.removePage.replace("__PAGE_ID__", id);

						$.ajax({
							method: "DELETE",
							url: url
						}).done(function() {
							console.log("remove page!", id);
							group.remove();
						}).fail(function() {
							alert("Une erreur est survenue, veuillez recharger la page.")
						});
					}
				});
			}
		});

	}

	initSortableItems(items){
		var self = this;
		Sortable.create(items, {
			group: 'item',
			animation: 250,
			filter: ".add",
			ghostClass: "ghost",
			chosenClass: "chosen",
			onAdd: function (evt) {
				if(evt.to.innerHTML != "" && ($(evt.to).parents("[data-sortable-manager-group]").attr("data-sortable-manager-group") == "add")){
					self.addGroup($(evt.to));
					evt.item.remove();
				}else{
					$("[data-sortable-manager-item-add]", items).appendTo($("[data-sortable-manager-item-add]", items).parent());

					var idContent = $(evt.item).attr("data-sortable-manager-item-id");
					var idPage = $(evt.item).parents("[data-sortable-manager-group]").attr("data-sortable-manager-group-id");
					var index = $(evt.item).index();

					console.log(idPage + " - " + idContent + " - " + index);

					var url = self.options.updateContentPosition.replace("__PAGE_ID__", idPage);
					url = url.replace("__CONTENT_ID__", idContent);
					url = url.replace("__INDEX__", index);
					$.ajax({
						method: "POST",
						url: url
					}).done(function() {
						console.log("update position content!");
					}).fail(function() {
						alert("Une erreur est survenue, veuillez recharger la page.");
					});
				}
			},
			onUpdate: function (evt) {
				$("[data-sortable-manager-item-add]", items).appendTo($("[data-sortable-manager-item-add]", items).parent());

				var idContent = $(evt.item).attr("data-sortable-manager-item-id");
				var idPage = $(evt.item).parents("[data-sortable-manager-group]").attr("data-sortable-manager-group-id");
				var index = $(evt.item).index();

				console.log(idPage + " - " + idContent + " - " + index);

				var url = self.options.updateContentPosition.replace("__PAGE_ID__", idPage);
				url = url.replace("__CONTENT_ID__", idContent);
				url = url.replace("__INDEX__", index);
				$.ajax({
					method: "POST",
					url: url
				}).done(function() {
					console.log("update position content!");
				}).fail(function() {
					alert("Une erreur est survenue, veuillez recharger la page.");
				});
			},
			onSort: function (evt) {
				
			},
			onMove: function (evt) {
				var group = $(evt.related).parents("[data-sortable-manager-group]");

				if(typeof $(evt.related).attr("data-sortable-manager-item-add") == "undefined" || 
					group.attr("data-sortable-manager-group") == "add"){
					//continue
				}else{
					if(group.attr("data-sortable-manager-group") == "normal" && group.find("[data-sortable-manager-item]").length == 0){
						//continue
					}else{
						return false;
					}
				}
			}
		});
	}

	addGroup(to){

		console.log("Create group!");

		var group = $(to).parents("[data-sortable-manager-group]:eq(0)").clone();

		$("[data-sortable-manager-item-add]", group).appendTo($("[data-sortable-manager-item-add]", group).parent());

		group.removeClass('add');
		group.attr("data-sortable-manager-group", "normal");
		$(to).parents("[data-sortable-manager-group]").before(group);
		this.initSortableItems($("[data-sortable-manager-items]", group)[0]);

		var newItem = $("[data-sortable-manager-item]", group);

		var idContent = newItem.attr("data-sortable-manager-item-id");
		var idPage = newItem.parents("[data-sortable-manager-group]").attr("data-sortable-manager-group-id");
		var index = newItem.index();

		var idPage = group.attr("data-sortable-manager-group-id");
		var groupIndex = 0;

		var groups = $("[data-sortable-manager-group='normal']").toArray();

		for(let group in groups){					
			if($(groups[group]).attr("data-sortable-manager-group-id") == idPage){
				break;
			}

			groupIndex++;
		}

		$("[data-sortable-manager-group-title]", group).text(this.options.defaultPageName);

		console.log(idPage + " - " + idContent + " - " + index + " - " + groupIndex);

		var url = this.options.autoAddNewPage.replace("__CONTENT_ID__", idContent);
		url = url.replace("__POSITION__", groupIndex);
		$.ajax({
			method: "POST",
			url: url,
			dataType:"json"
		}).done(function(result) {
			console.log("new page! " + result.id);
			group.attr("data-sortable-manager-group-id", result.id);
		}).fail(function() {
			alert("Une erreur est survenue, veuillez recharger la page.");
		});
	}
}