jQuery(document).ready(function($) {
	var sortableManager = new SortableManager($("[data-sortable-manager]"), {
		addNewPageLink:"",
		removePage:"",
		removePageConfirmFct:function(callback){
			callback(true);
		},
		autoAddNewPage:"",
		moveContentToPage:"",
		editPageLink:"",
		addNewContentLink:"",
		removeContent:"",
		removeContentConfirmFct:function(callback){
			callback(true);
		},
		editContentLink:"",
		updatePagePosition:"",
		updateContentPosition:"",
		defaultPageName:"New page"
    });
});