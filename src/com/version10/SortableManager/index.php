﻿<?php 
	$v = "?v=1";
?>
<!doctype html>
<html lang="fr">

	<head>
		<meta charset="utf-8"/>
		<meta name="msapplication-tap-highlight" content="no">
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<meta name="" content="content">
		<meta name="description" content=".">
		<meta name="keywords" content="" />
		<title>R&D</title>

		<!-- Stylesheet -->
		<link rel="stylesheet" href="../../../../styles/css/styles.min.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="../../../../bower_components/syntaxhighlighter/styles/shCoreRDark.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="../../../../bower_components/syntaxhighlighter/styles/shThemeRDark.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="styles/css/SortableManager.min.css<?= $v ?>" type="text/css" media="screen, print"/>
		<link rel="stylesheet" href="styles/css/main.min.css<?= $v ?>" type="text/css" media="screen, print"/>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	</head>

	<body>
		<h1 id="title-page"><span class="logo"></span>SortableManager<span class="btn-back" data-href="./../../../../"></span></h1>

		<nav id="filters-menu" data-filters-menu></nav>
			
		<section id="container">
			<!-- Quick start -->
			<h2 class="title-section">Quick start</h2>
			<div class="box">

				<h3 class="title-sub">Dependencies</h3>

				<div class="sublist">
					jQuery / GSAP
					<div class="code">
						<pre class="brush: js">
							/**
							* Run in bower
							*/
							bower install jquery gsap Sortable --save
						</pre>
					</div>
				</div>

				<h3 class="title-sub">Download</h3>

				<div class="sublist">
					Build
					<div class="code">
						<pre class="brush: js">
							/**
							* Run in bower
							*/
							bower install https://YOUR_NAME@bitbucket.org/version10/r-d.git#SortableManager --save
						</pre>
					</div>
				</div>

				<h3 class="title-sub">Installation</h3>

				<div class="sublist">
					Add script
					<div class="code">
						<script type="syntaxhighlighter" class="brush: html"><![CDATA[
							/**
							* Dependencies
							*/
							<script src="bower_components/jquery/dist/jquery.min.js" />
							<script src="bower_components/gsap/src/minified/TweenMax.min.js" />
							<script src="bower_components/gsap/src/minified/easing/EasePack.min.js" />
							<script src="bower_components/gsap/src/minified/plugins/CSSPlugin.min.js" />
							<script src="bower_components/Sortable/Sortable.min.js" />

							/**
							* SortableManager script
							*/
							<script type="text/javascript" src="SortableManager.min.js" />
						]]></script>
					</div>
				</div>

				<div class="sublist">
					Add style
					<div class="code">
						<script type="syntaxhighlighter" class="brush: html"><![CDATA[
							/**
							* Inner head
							*/
							<link rel="stylesheet" href="SortableManager.min.css" type="text/css" media="screen, print" />
						]]></script>
					</div>
				</div>

				<div class="sublist">
					HTML
					<div class="code">
						<script type="syntaxhighlighter" class="brush: html"><![CDATA[
							/**
							* Inner body
							*/
							<div class="sortable-manager-cnt" data-sortable-manager>
								<div class="group" data-sortable-manager-group="normal" data-sortable-manager-group-id="1" data-sortable-manager-group-layout="full_page">
									<a class="btn-edit" data-sortable-manager-btn-group-edit><i class="fa fa-pencil"></i></a>
									<a class="btn-remove" data-sortable-manager-btn-group-remove><i class="fa fa-trash"></i></a>
									<div class="title" data-sortable-manager-group-title>Page 1</div>
									<div class="items" data-sortable-manager-items>
										<div class="item" data-sortable-manager-item data-sortable-manager-item-id="1">
											<div class="fullbox">
												<p>1</p>
											</div>
											<div class="nav">
												<a class="btn-edit" data-sortable-manager-btn-item-edit><i class="fa fa-pencil"></i></a>
												<a class="btn-remove" data-sortable-manager-btn-item-remove><i class="fa fa-trash"></i></a>
											</div>
										</div>
										<div class="item add" data-sortable-manager-item-add>
											<div class="fullbox">
												<a class="btn-add" data-sortable-manager-btn-item-add><i class="fa fa-plus"></i></a>
											</div>
										</div>
									</div>
								</div>
								<div class="group add" data-sortable-manager-group="add">
									<a class="btn-edit" data-sortable-manager-btn-group-edit><i class="fa fa-pencil"></i></a>
									<a class="btn-remove" data-sortable-manager-btn-group-remove><i class="fa fa-trash"></i></a>
									<div class="title" data-sortable-manager-group-title>&nbsp;</div>
									<div class="items" data-sortable-manager-items>
										<div class="item add" data-sortable-manager-item-add>
											<div class="fullbox">
												<a class="btn-add" data-sortable-manager-btn-item-add><i class="fa fa-plus"></i></a>
											</div>
										</div>
									</div>
									<a class="btn-add" data-sortable-manager-btn-add><i class="fa fa-plus"></i></a>
								</div>
							</div>
						]]></script>
					</div>
				</div>

				<h3 class="title-sub">First Call</h3>

				<div class="code">
					<script type="syntaxhighlighter" class="brush: js"><![CDATA[
						/**
						* Initialisation
						*/
						var sortableManager = new SortableManager($("[data-sortable-manager]"));


					]]></script>
				</div>
			</div>

			<!-- Options -->
			<h2 class="title-section">Options</h2>
			<div class="box">
				<h3 class="title-sub">Usage</h3>

				<div class="sublist">
					Exemple
					<div class="code">
						<script type="syntaxhighlighter" class="brush: js"><![CDATA[
						/**
						* Exemple
						*/
						var sortableManager = new SortableManager($("[data-sortable-manager]"), {
							addNewPageLink:"",
							removePage:"",
							autoAddNewPage:"",
							editPageLink:"",
							addNewContentLink:"",
							removeContent:"",
							editContentLink:"",
							updatePagePosition:"",
							updateContentPosition:""
						});
					]]></script>
					</div>
				</div>

				<h3 class="title-sub">Parameters</h3>

				<div class="sublist">
					Options of your RainTransition
					<div class="code">
						<table class="params">
							<thead>
								<tr>
									<th>Name</th>
									<th>Type</th>
									<th>Default</th>
									<th class="last">Description</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="name"><code>addNewPageLink</code></td>
									<td class="type">string</td>
									<td class="default">""</td>
									<td class="description">The link to add new page</td>
								</tr>
								<tr>
									<td class="name"><code>removePage</code></td>
									<td class="type">string</td>
									<td class="default">""</td>
									<td class="description">WS to remove page</td>
								</tr>
								<tr>
									<td class="name"><code>autoAddNewPage</code></td>
									<td class="type">string</td>
									<td class="default">""</td>
									<td class="description">WS to auto add new page</td>
								</tr>
								<tr>
									<td class="name"><code>addNewContentLink</code></td>
									<td class="type">string</td>
									<td class="default">""</td>
									<td class="description">The link to add new content</td>
								</tr>
								<tr>
									<td class="name"><code>removeContent</code></td>
									<td class="type">string</td>
									<td class="default">""</td>
									<td class="description">WS to remove content</td>
								</tr>
								<tr>
									<td class="name"><code>editPageLink</code></td>
									<td class="type">string</td>
									<td class="default">""</td>
									<td class="description">The link to edit page</td>
								</tr>
								<tr>
									<td class="name"><code>updatePagePosition</code></td>
									<td class="type">string</td>
									<td class="default">""</td>
									<td class="description">WS to update the page position</td>
								</tr>
								<tr>
									<td class="name"><code>updateContentPosition</code></td>
									<td class="type">string</td>
									<td class="default">""</td>
									<td class="description">WS to update the content position</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- Examples -->
			<h2 class="title-section">Examples</h2>
			<div class="box">
				<div class="sortable-manager-cnt" data-sortable-manager>
					<div class="group" data-sortable-manager-group="normal" data-sortable-manager-group-id="1" data-sortable-manager-group-layout="full_page">
						<a class="btn-group-edit" data-sortable-manager-btn-group-edit><i class="fa fa-pencil"></i></a>
						<a class="btn-group-remove" data-sortable-manager-btn-group-remove><i class="fa fa-trash"></i></a>
						<div class="title" data-sortable-manager-group-title>Page 1</div>
						<div class="items" data-sortable-manager-items>
							<div class="item" data-sortable-manager-item data-sortable-manager-item-id="1">
								<div class="fullbox">
									<p>1</p>
								</div>
								<div class="nav">
									<a class="btn-edit" data-sortable-manager-btn-item-edit><i class="fa fa-pencil"></i></a>
									<a class="btn-remove" data-sortable-manager-btn-item-remove><i class="fa fa-trash"></i></a>
								</div>
							</div>
							<div class="item" data-sortable-manager-item data-sortable-manager-item-id="2">
								<div class="fullbox">
									<p>2</p>
								</div>
								<div class="nav">
									<a class="btn-edit" data-sortable-manager-btn-item-edit><i class="fa fa-pencil"></i></a>
									<a class="btn-remove" data-sortable-manager-btn-item-remove><i class="fa fa-trash"></i></a>
								</div>
							</div>
							<div class="item" data-sortable-manager-item data-sortable-manager-item-id="3">
								<div class="fullbox">
									<p>3</p>
								</div>
								<div class="nav">
									<a class="btn-edit" data-sortable-manager-btn-item-edit><i class="fa fa-pencil"></i></a>
									<a class="btn-remove" data-sortable-manager-btn-item-remove><i class="fa fa-trash"></i></a>
								</div>
							</div>
							<div class="item add" data-sortable-manager-item-add>
								<div class="fullbox">
									<a class="btn-add" data-sortable-manager-btn-item-add><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="group" data-sortable-manager-group="normal" data-sortable-manager-group-id="2" data-sortable-manager-group-layout="full_page">
						<a class="btn-group-edit" data-sortable-manager-btn-group-edit><i class="fa fa-pencil"></i></a>
						<a class="btn-group-remove" data-sortable-manager-btn-group-remove><i class="fa fa-trash"></i></a>
						<div class="title" data-sortable-manager-group-title>Page 1</div>
						<div class="items" data-sortable-manager-items>							
							<div class="item add" data-sortable-manager-item-add>
								<div class="fullbox">
									<a class="btn-add" data-sortable-manager-btn-item-add><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="group add" data-sortable-manager-group="add" data-sortable-manager-group-id="null">
						<a class="btn-group-edit" data-sortable-manager-btn-group-edit><i class="fa fa-pencil"></i></a>
						<a class="btn-group-remove" data-sortable-manager-btn-group-remove><i class="fa fa-trash"></i></a>

						<div class="title" data-sortable-manager-group-title>&nbsp;</div>
						<div class="items" data-sortable-manager-items>
							<div class="item add" data-sortable-manager-item-add>
								<div class="fullbox">
									<a class="btn-add" data-sortable-manager-btn-item-add><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</div>
						<a class="btn-add" data-sortable-manager-btn-group-add><i class="fa fa-plus"></i></a>
					</div>
				</div>
			</div>

		</section>

		<!-- Global Vars -->
		<script>
			var VERSION = "<?= $v ?>";
		</script>
		<!-- Global Vars -->

		

		<!-- Depencencies -->
		<script src="../../../../bower_components/jquery/dist/jquery.min.js<?= $v ?>"></script>

		<!-- Depencencies R&D -->
		<script src="../../../../bower_components/isotope/dist/isotope.pkgd.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/XRegExp.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shCore.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushJScript.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushPhp.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushCss.js<?= $v ?>"></script>
		<script src="../../../../bower_components/syntaxhighlighter/scripts/shBrushXml.js<?= $v ?>"></script>

		<!-- Depencencies -->
		<script src="../../../../bower_components/gsap/src/minified/TweenMax.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/gsap/src/minified/easing/EasePack.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/gsap/src/minified/plugins/CSSPlugin.min.js<?= $v ?>"></script>
		<script src="../../../../bower_components/Sortable/Sortable.min.js<?= $v ?>"></script>

		<!-- Scripts R&D -->
		<script type="text/javascript" src="../../../../scripts/dist/scripts.min.js<?= $v ?>"></script>
		<!-- Scripts -->
		<script type="text/javascript" src="scripts/dist/scripts.min.js<?= $v ?>"></script>
		<script type="text/javascript" src="scripts/js/main.js<?= $v ?>"></script>
	</body>
</html>
