jQuery(document).ready(function($) {

	//Btn back
	$("body").on("click", "[data-href]", function(e){
		e.preventDefault();
		window.location = $(this).attr("data-href");
	});

	//Link
	$("body").on("click", "[data-href]", function(e){
		e.preventDefault();
		window.location = $(this).attr("data-href");
	});

	$(document).on("highlight_complete", function(){
		initIsotope();
	});

	SyntaxHighlighter.all();

	function initIsotope(){
		//Search Isotop
		var qsRegex;
		//var buttonFilter;

		// init Isotope
		var isotope = $("[data-isotope]").isotope({
			itemSelector: ".grid-item",
			masonry: {
				columnWidth: '.grid-sizer'
			},
			filter: function() {
				var $this = $(this);
				var searchResult = qsRegex ? $this.text().match(qsRegex) : true;
				//var buttonResult = buttonFilter ? $this.is(buttonFilter) : true;
				return searchResult /*&& buttonResult*/;
			}
		});

		//Search field to filter
		var $quicksearch = $("[data-input-search]").keyup(debounce(function() {
			qsRegex = new RegExp($quicksearch.val(), "gi");
			isotope.isotope();
		}));
	}
});

//Debounce so filtering doesn't happen every millisecond
function debounce(fn, threshold) {
	var timeout;
	return function debounced() {
		if (timeout) {
			clearTimeout(timeout);
		}
		function delayed() {
			fn();
			timeout = null;
		}
		setTimeout(delayed, threshold || 100);
	};
}